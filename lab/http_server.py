# -*- encoding: utf-8 -*-

import socket


def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                print "Odebrano:"
                print request
                # Wysłanie zawartości strony
                h = "HTTP/1.1 200 OK Host: localhost:4444\r\n"
                h+="Content-Type: text/plain\r\n"
                connection.sendall(h+html)
                #print connection.recv(1024)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4444)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

